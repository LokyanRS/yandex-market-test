package com.lokyanrs;

import io.qameta.allure.Step;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

public class YandexMarketTest {
    private static String elementWithText = "(//*[text()='%s'])[1]";

    private WebDriver driver;

    @Before
    @Step("Открыть браузер и развернуть на весь экран")
    public void setUp() {
        // Очищаем кэш
        InternetExplorerOptions ieOptions = new InternetExplorerOptions().destructivelyEnsureCleanSession();

        System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
        driver = new InternetExplorerDriver(ieOptions);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @Test
    public void marketTest() {
        String searchSecondResult;

        openUrl("https://www.yandex.ru/");
        openMarket();
        hoverComputerSection();
        clickTabletSection();
        clickAdvancedSearch();
        setPrice("20000", "25000");
        choseManufacturer("ASUS");
        acceptSearch();

        searchSecondResult = getSearchSecondResult();
        setSearchField(searchSecondResult);
        checkProductTitle(searchSecondResult);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Step("Зайти на {url}")
    private void openUrl(String url) {
        driver.get(url);
    }

    @Step("Перейти на Яндекс Маркет")
    private void openMarket() {
        driver.findElement(By.xpath("//a[@data-id='market']")).click();
    }

    @Step("Выбрать раздел Компьютеры")
    private void hoverComputerSection() {
        WebElement element = driver.findElement(By.xpath(String.format(elementWithText, "Компьютеры")));
        Actions action = new Actions(driver);
        action.moveToElement(element).build().perform();
    }

    @Step("Выбрать раздел Планшеты")
    private void clickTabletSection() {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(String.format(elementWithText, "Планшеты")))).click();
    }

    @Step("Зайти в расширенный поиск")
    private void clickAdvancedSearch() {
        WebElement element = driver.findElement(By.xpath(String.format(elementWithText, "Все фильтры")));
        Actions action = new Actions(driver);
        action.moveToElement(element).perform();
        element.click();
    }

    @Step("Задать параметр поиска от {from} до {to} рублей")
    private void setPrice(String from, String to) {
        driver.findElement(By.id("glf-pricefrom-var")).sendKeys(from);
        driver.findElement(By.id("glf-priceto-var")).sendKeys(to);
    }

    @Step("Выбрать производителя {manufacturer}")
    private void choseManufacturer(String manufacturer) {
        driver.findElement(By.xpath(String.format(elementWithText, manufacturer))).click();
    }

    @Step("Применить условия поиска")
    private void acceptSearch() {
        WebElement element = driver.findElement(By.xpath(String.format(elementWithText, "Показать подходящие")));
        Actions action = new Actions(driver);
        action.moveToElement(element).perform();
        element.click();
    }

    @Step("Запомнить второй элемент в результатах поиска")
    private String getSearchSecondResult() {
        return driver.findElement(By.xpath("(//div[contains(@data-bem, '\"name\":\"snippet-list\"')]/descendant::a[contains(@class, 'link_theme_blue') and @title])[2]"))
                .getAttribute("title");

    }

    @Step("Ввести значение в поисковую строку ({searchValue})")
    private void setSearchField(String searchValue) {
        driver.findElement(By.id("header-search")).sendKeys(searchValue);
        driver.findElement(By.xpath(String.format(elementWithText, "Найти"))).click();
    }

    @Step("Проверить, что наименование товара соответствует запомненному")
    private void checkProductTitle(String expectedValue) {
        String realValue = driver.findElement(By.xpath("//div[@class='n-product-summary__headline']/descendant::h1[contains(@class, 'title')]")).getText();
        assertEquals(expectedValue, realValue);
    }
}
